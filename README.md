# Reconstruction of spectral functions using a stochastic maximum entropy method

## Development Process

This programs uses the [Eigen](https://eigen.tuxfamily.org/) library.
You may need to edit [Makefile](Makefile), to point `EIGEN_PATH` in the correct direction.
Then, build and choose an example to run.

```
# Clone the repo:
git clone https://gitlab.com/path_integral_mc/spectrum-reconstructor.git
cd spectrum-reconstructor

# Build:
make

# Run:
cd build
./main
```

## License

Copyright (C) 2020,2021 Efremkin, V.

Spectrum reconstructor is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Spectrum reconstructor is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You have a copy of the GNU General Public License in the file [COPYING](COPYING). You can also see [GPL 3.0+](http://www.gnu.org/licenses/gpl-3.0.en.html).
