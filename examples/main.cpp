#include <fstream>
#include <sstream>
#include <iostream>

#include "MC_calc.h"

void reading_configurations_single(VectorXd& arr, std::ifstream& in);

int main() {
	const double THETA = 5.;
	const double MU = 10;
	const double OMEGA_MIN = 0., OMEGA_MAX = 5.;
	const u_int OMEGA_NUMBER = 50;

	const double BETA_eff = 1;
	const u_int N_TAU = 50;

	const bool VERBOSE = true;

	const u_int TOTAL_NUMBER_OF_ITERATIONS = 1e8;
	const double PROB_SINGLE = 0.2;
	const double PROB_MANY = 0.3;

	// INITIALIZATION
	VectorXd CORRELATION_FUNCTION_DATA(N_TAU+1), ERROR_DATA(N_TAU+1);

	// Reading from files
	std::string name = "INPUT_CORRELATION_FUNCTION_b=1_M=50_exact.txt";
	std::ifstream input_file(name);
	reading_configurations_single(CORRELATION_FUNCTION_DATA, input_file);
	
	input_file.close();
	name = "INPUT_ERRORS_b=1_M=50.txt";
	input_file.open(name);
	reading_configurations_single(ERROR_DATA, input_file);
	//	std::cout << "corr \t and erros \n" << CORRELATION_FUNCTION.transpose() << "\t" << ERRORS.transpose() << std::endl;

	// Write in file
	std::string name_out = "Sp_fun_punish_term_v1_sombrero_exact_mu=" + std::to_string(MU) + "_b=1_M=50_al=1_om=" + std::to_string(OMEGA_NUMBER) + "_th=" + std::to_string(THETA) + ".txt";
	std::ofstream output(name_out);

	MC_calc spectrum(CORRELATION_FUNCTION_DATA, ERROR_DATA, THETA, MU, OMEGA_MIN, OMEGA_MAX, OMEGA_NUMBER, BETA_eff, N_TAU, VERBOSE);
	spectrum.run(TOTAL_NUMBER_OF_ITERATIONS, PROB_SINGLE, PROB_MANY, output);

	return 0;
}

void reading_configurations_single(VectorXd& arr, std::ifstream& in)
{
	double value;
	u_int COUNTER = 0;
	while (in)
	{
		in >> value;
		arr[COUNTER] = value;
		if (COUNTER < arr.size()-1) ++COUNTER;
		else break;
	}
}
