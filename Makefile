# Reconstruction of spectral functions using a stochastic maximum entropy method
# Copyright (C) 2020,2021 Efremkin, V.

# This file is part of Spectrum reconstructor.

# Spectrum reconstructor is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Spectrum reconstructor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Spectrum reconstructor. If not, see <http://www.gnu.org/licenses/>

SHELL			= /bin/sh

# Paths & Includes
OUTDIR			= $(CURDIR)/build
SRCDIR			= $(CURDIR)/src
EXAMPLESDIR		= $(CURDIR)/examples
VPATH			= $(SRCDIR):$(EXAMPLESDIR)
EIGEN_PATH		= /usr/include/eigen3
INCLUDES		= -I$(SRCDIR) -I$(EIGEN_PATH)

LDFLAGS			=
LDLIBS			= -lm
WARN			= -Wall -Wextra
OPTIMIZATION		= -O3 -march=native
CXX			= g++
CPPFLAGS		= $(INCLUDES)
CXXFLAGS		= $(OPTIMIZATION) $(WARN) -std=c++17
DEPFLAGS		= -MT $@ -MMD -MP -MF $(OUTDIR)/$*.d
COMPILE.cc		= $(CXX) $(DEPFLAGS) $(CXXFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c

SOURCES_SRC		= $(shell cd $(SRCDIR) && ls *.cpp)
EXAMPLES_SRC		= $(shell cd $(EXAMPLESDIR) && ls *.cpp)
EXAMPLES		= $(EXAMPLES_SRC:.cpp=)
DEPS			:= $(SOURCES_SRC:%.cpp=$(OUTDIR)/%.d) $(EXAMPLES_SRC:%.cpp=$(OUTDIR)/%.d)
# DEPS is explained in https://make.mad-scientist.net/papers/advanced-auto-dependency-generation/

# Rules
.PHONY: all clean

all: $(patsubst %, $(OUTDIR)/%, $(EXAMPLES))

$(DEPS):
include $(wildcard $(DEPS))

$(OUTDIR)/%.o : %.cpp $(OUTDIR)/%.d | $(OUTDIR)
	$(COMPILE.cc) -o $@ $<

$(OUTDIR): ; @mkdir -p $@

clean:
	rm -fv $(OUTDIR)/*.d $(OUTDIR)/*.o $(patsubst %, $(OUTDIR)/%, $(EXAMPLES))
	rmdir $(OUTDIR)

$(OUTDIR)/main: $(patsubst %, $(OUTDIR)/%, main.o MC_calc.o)
	$(LINK.cc) -o $@ $^ $(LDLIBS)
