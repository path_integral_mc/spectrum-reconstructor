#pragma once
#include <Eigen/Dense>
#include <fstream>
#include <random>
#include <tuple>

using Eigen::VectorXd;
using namespace Eigen;

class MC_calc
{
protected:
	VectorXd tau_vec;
	VectorXd omega_vec;
	VectorXd CORR_FUNC_ref, ERRORS;
	VectorXd A, A_trial;
	MatrixXd kernel;
	// Parameters for the correlation function
	const double THETA;
	const double MU;
	const double OMEGA_MIN;
	const double OMEGA_MAX;
	const u_int OMEGA_NUMBER;
	const double DELTA_OMEGA;
	const double BETA_eff;
	const u_int N_TAU;
	const double DELTA_TAU;
	// Random number generators
	std::random_device device;
	std::mt19937 gen;
	std::uniform_real_distribution<double> un_dis_01; // RNG in [0.0, 1.0)
	std::uniform_int_distribution<> dis_omega;
	std::uniform_real_distribution<double> un_dis_pm_05; // RNG in [-0.5, 0.5]
	// Paramenters/acumulators for the MC
	double CHI2 = 0, CHI2_trial = 0, SITE_SUM = 0, SITE_SUM_trial = 0;
	const double displ_1, displ_all;
	const bool VERBOSE;

	void update_kernel();
	void set_initial_values_of_A();
	bool single_coef_upd();
	bool two_coef_upd();
	bool many_coef_upd();
	bool Metropolis();
public:
	MC_calc(const VectorXd& corr, const VectorXd& errors, double _THETA, double _MU, double _OMEGA_MIN, double _OMEGA_MAX, u_int _OMEGA_NUMBER, double _BETA_eff, u_int _N_TAU, bool _VERBOSE);
	void run(u_int _TOTAL_NUMBER_OF_ITERATIONS, double _PROB_SINGLE, double _PROB_MANY, std::ofstream& out_mc);
	std::tuple<double, double> get_chi2(const VectorXd& amp);
	double fermi_factor(double energy);
	void print_spectrum(std::ofstream& out_mc, const std::string eol = "\n");
};

