#include "MC_calc.h"

#include <cmath>
#include <iostream>
#include <fstream>

MC_calc::MC_calc(const VectorXd& corr, const VectorXd& errors, double _THETA, double _MU, double _OMEGA_MIN, double _OMEGA_MAX, u_int _OMEGA_NUMBER, double _BETA_eff, u_int _N_TAU, bool _VERBOSE) :
	CORR_FUNC_ref(corr), ERRORS(errors),
	THETA(_THETA), MU(_MU), OMEGA_MIN(_OMEGA_MIN), OMEGA_MAX(_OMEGA_MAX), OMEGA_NUMBER(_OMEGA_NUMBER),
	DELTA_OMEGA((_OMEGA_MAX - _OMEGA_MIN) / _OMEGA_NUMBER),
	BETA_eff(_BETA_eff), N_TAU(_N_TAU),
	DELTA_TAU(_BETA_eff / _N_TAU),
	gen(device()), un_dis_01(0.0, 1.0), dis_omega(0, _OMEGA_NUMBER - 1), un_dis_pm_05(-0.5, std::nextafter(0.5, std::numeric_limits<double>::max())),
	displ_1(0.005 / sqrt(_THETA)), displ_all(0.01/ sqrt(_THETA)),
	VERBOSE(_VERBOSE)
{
	if (VERBOSE)
		std::cout << "# THETA = " << THETA << '\n'
			<< "# MU = " << MU << '\n'
			<< "# OMEGA_MIN = " << OMEGA_MIN << '\n'
			<< "# OMEGA_MAX = " << OMEGA_MAX << '\n'
			<< "# OMEGA_NUMBER = " << OMEGA_NUMBER << '\n'
			<< "# DELTA_OMEGA = " << DELTA_OMEGA << '\n'
			<< "# BETA_eff = " << BETA_eff << '\n'
			<< "# N_TAU = " << N_TAU << '\n'
			<< "# DELTA_TAU = " << DELTA_TAU << std::endl;
	tau_vec = VectorXd::LinSpaced(N_TAU+1, 0.0, N_TAU*DELTA_TAU);
	omega_vec = VectorXd::LinSpaced(OMEGA_NUMBER, 0.0, (OMEGA_NUMBER-1)*DELTA_OMEGA);
	update_kernel();
	A_trial = VectorXd::Zero(OMEGA_NUMBER);
	set_initial_values_of_A();
	std::tie(CHI2, SITE_SUM) = get_chi2(A);
}
void MC_calc::run(u_int _TOTAL_NUMBER_OF_ITERATIONS, double _PROB_SINGLE, double _PROB_MANY, std::ofstream& out_mc)
{
	const u_int TOTAL_NUMBER_OF_ITERATIONS(_TOTAL_NUMBER_OF_ITERATIONS);
	const double PROB_SINGLE(_PROB_SINGLE);
	const double PROB_MANY(_PROB_MANY);
	u_int accepted_mc = 0;
	u_int counter_single = 0, accepted_single = 0;
	u_int counter_many = 0, accepted_many = 0;
	u_int counter_two = 0, accepted_two = 0;
	for (u_int iter = 0; iter < TOTAL_NUMBER_OF_ITERATIONS; ++iter)
	{
		A_trial = A;
		bool success_upd;
		const double u = un_dis_01(gen);
		if (u < PROB_SINGLE) {
			++counter_single;
			success_upd = single_coef_upd();
		}
		else if (u < PROB_SINGLE + PROB_MANY) {
			++counter_many;
			success_upd = many_coef_upd();
		}
		
		else {
			++counter_two;
			success_upd = two_coef_upd();
		}

		if (success_upd) {
			if (Metropolis()) {
				++accepted_mc;
				if (u < PROB_SINGLE)
					++accepted_single;
				else if (u < PROB_SINGLE + PROB_MANY)
					++accepted_many;
				else
					++accepted_two;
			}
		}

		if (!(iter % 500)) {
			print_spectrum(out_mc);
			out_mc << "# " << CHI2 << '\t' << SITE_SUM << '\n';
			out_mc << "# Acceptance: " << (double) accepted_mc/(iter+1) << '\t'
				<< "Single: " << (double) accepted_single/counter_single << '\t'
				<< "Many: " << (double) accepted_many/counter_many << '\t'
				<< "Two: " << (double) accepted_two/counter_two << std::endl;
			if (VERBOSE) {
				std::cout << "# chi2: " << CHI2 << "\t sum: " << SITE_SUM << '\n';
				std::cout << "# Acceptance: " << (double) accepted_mc/(iter+1) << '\t'
					<< "Single: " << (double) accepted_single/counter_single << '\t'
					<< "Many: " << (double) accepted_many/counter_many << '\t'
					<< "Two: " << (double) accepted_two/counter_two << std::endl;
			}
		}

	}
}
// UPDATING ROUTINES
// One coefficient move
bool MC_calc::single_coef_upd()
{
	const u_int omega_place = dis_omega(gen);
	A_trial(omega_place) = A(omega_place) + displ_1 * un_dis_pm_05(gen);
	if (A_trial(omega_place) < 0)
		return false;
	else
		return true;
}
// Two coefficients move
bool MC_calc::two_coef_upd()
{
	const u_int omega_place1 = dis_omega(gen);
	u_int omega_place2;
	if (omega_place1 == OMEGA_NUMBER - 1)
		omega_place2 = 0;
	else
		omega_place2 = omega_place1 + 1;
	A_trial(omega_place1) = A(omega_place2);
	A_trial(omega_place2) = A(omega_place1);
	return true;
}
// Change in 1 A distributed equally among all others
bool MC_calc::many_coef_upd()
{
	const u_int omega_place = dis_omega(gen);
	const double A_delta = displ_all * un_dis_pm_05(gen);
	const double SUM = A.sum();
	if (A_delta > 0) {
		A_trial = A / (1 + A_delta);
		A_trial[omega_place] += A_delta * SUM / (1 + A_delta);
	}
	else {
		A_trial = (1 - A_delta) * A;
		A_trial[omega_place] += A_delta * SUM;
	}

	if (A_trial.array().minCoeff() < 0 )
	    return false;
	else
	    return true;
}
bool MC_calc::Metropolis()
{
	std::tie(CHI2_trial, SITE_SUM_trial) = get_chi2(A_trial);
	const double action_difference = THETA * (CHI2_trial - CHI2) + THETA * MU * (SITE_SUM_trial - SITE_SUM);
	const long double S = exp(-action_difference);
	
	if ( S > un_dis_01(gen) )
	{
		A = A_trial;
		SITE_SUM = SITE_SUM_trial;
		CHI2 = CHI2_trial;
		return true;
	}
	return false;
}
void MC_calc::set_initial_values_of_A()
{
	A = VectorXd::Constant(OMEGA_NUMBER, 1. / OMEGA_NUMBER);
}
void MC_calc::update_kernel()
{
	const MatrixXd exp_tau_omega = (tau_vec * omega_vec.transpose()).array().exp();
	const MatrixXd exp_m_tau_omega = exp_tau_omega.cwiseInverse();
	const MatrixXd exp_m_beta_omega = (VectorXd::Constant(N_TAU+1, -BETA_eff) * omega_vec.transpose()).array().exp();
	kernel = (exp_m_tau_omega + exp_m_beta_omega.cwiseProduct(exp_tau_omega)).cwiseQuotient((1+exp_m_beta_omega.array()).matrix());
}
std::tuple<double, double> MC_calc::get_chi2(const VectorXd& amp)
{
	const double energy_fermi = 0.01;
	const double site_sum = (1. / (((-1 * amp.array() + energy_fermi) / 0.01).exp() + 1.)).sum();

	const VectorXd CORR_test = kernel * amp;

	const double chi2 = (CORR_FUNC_ref - CORR_test).cwiseQuotient(ERRORS).squaredNorm();
	return {chi2, site_sum};
}
void MC_calc::print_spectrum(std::ofstream& out_mc, const std::string eol)
{
	IOFormat TabSeparated(StreamPrecision, DontAlignCols, "\t", "\n", "", "", "", "");
	out_mc << A.transpose().format(TabSeparated) << eol;

	if (VERBOSE) {
		std::cout << "# Coeff  = \n" << A.transpose().format(TabSeparated) << eol;
	}
}
double MC_calc::fermi_factor(double energy)
{
	const double energy_fermi = 0.01;
	return 1. / (exp((-energy + energy_fermi) / 0.01) + 1.);
}
